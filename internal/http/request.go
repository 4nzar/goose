package http

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/4nzar/goose/internal/model"
	// goose_slice "gitlab.com/4nzar/goose/pkg/slice"
)

func setQueryParameters(opts *model.RequestOptions, u *url.URL) string {
	queries := u.Query()
	for k, v := range opts.QueryParameters {
		queries.Set(k, strings.Join(v, ","))
	}
	return queries.Encode()
}

func Request(opts *model.RequestOptions) (*http.Response, error) {
	client := http.Client{}
	u, err := url.Parse(opts.URL)
	if err != nil {
		return nil, err
	}
	if opts.TimeOut != nil {
		timeout := *opts.TimeOut
		c, cancel := context.WithTimeout(opts.Ctx, timeout*time.Second)
		defer cancel()
		opts.Ctx = c
	}
	u.RawQuery = setQueryParameters(opts, u)
	request, err := http.NewRequestWithContext(opts.Ctx, string(opts.Method), u.String(), opts.Body)
	if err != nil {
		return nil, err
	}
	for k, v := range opts.Headers {
		request.Header.Set(k, v)
	}
	log.Printf("%s %s\n", opts.Method, request.URL.String())
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	if response == nil {
		return nil, fmt.Errorf("error: calling %s returned empty response", u.String())
	}
	return response, nil
}
