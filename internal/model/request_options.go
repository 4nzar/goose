package model

import (
	"context"
	"io"
	"net/url"
	"time"
)

type HTTPMethod string

const (
	POST HTTPMethod = "POST"
	GET HTTPMethod = "GET"
	PUT HTTPMethod = "PUT"
	DELETE HTTPMethod = "DELETE"
	PATCH HTTPMethod = "PATCH"
	HEAD HTTPMethod = "HEAD"
	OPTION HTTPMethod = "OPTION"
)

type RequestOptions struct {
	URL             string
	Ctx             context.Context
	TimeOut         *time.Duration
	Method          HTTPMethod
	Headers         map[string]string
	QueryParameters url.Values
	Body            io.Reader
}

