package goose_slice

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMapSimpleTest(t *testing.T) {
	expected := []int{1, 4, 9, 16, 25}
	test := []int{1, 2, 3, 4, 5}
	got := Map(test, func(item int) int {
		return item * item
	})
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}

func TestMapSimpleTestFizzBuzz(t *testing.T) {
	expected := []string{"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz", "16", "17", "Fizz", "19", "Buzz"}
	test := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
	got := Map(test, func(item int) string {
		if item%3 == 0 && item%5 == 0 {
			return "FizzBuzz"
		}
		if item%3 == 0 {
			return "Fizz"
		}
		if item%5 == 0 {
			return "Buzz"
		}
		return strconv.Itoa(item)
	})
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}
