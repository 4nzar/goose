package goose_slice

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReduceSimpleTestShouldReturn10(t *testing.T) {
	expected := 10
	test := []int{1, 2, 3, 4}
	got := Reduce(test, func(agg, item int) int {
		return agg + item
	}, 0)
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}

func TestReduceSimpleTestShouldReturn204(t *testing.T) {
	expected := 204
	test := []int{1, 2, 3, 4, 5, 6, 7, 8}
	got := Reduce(test, func(agg, item int) int {
		return agg + int(math.Pow(float64(item), 2))
	}, 0)
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}

type OrderItem struct {
	Description string
	Quantity    float32
	Price       float32
}

func TestReduceShouldReturnTotalPrice(t *testing.T) {
	var expected float32
	expected = 178.0
	test := []OrderItem{
		{Description: "Eggs (Dozen)", Quantity: 1.0, Price: 3.0},
		{Description: "Cheese", Quantity: 0.5, Price: 5.0},
		{Description: "Butter", Quantity: 6.0, Price: 12.0},
	}
	got := Reduce(test, func(agg float32, item OrderItem) float32 {
		return agg + (item.Price * item.Price)
	}, 0)
	assert.Equalf(t, expected, got, "Expected '%f' got '%f'\n", expected, got)
}

func TestReduceWithIndexSimpleTestShouldReturn76(t *testing.T) {
	expected := 76
	test := []int{1, 2, 3, 4}
	got := ReduceWithIndex(test, func(agg, item, index int) int {
		return agg + int(math.Pow(float64(item), float64(index)))
	}, 0)
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}

func TestReduceWithIndexSimpleTestShouldReturn204(t *testing.T) {
	expected := 204
	test := []int{1, 2, 3, 4, 5, 6, 7, 8}
	got := ReduceWithIndex(test, func(agg, item, _ int) int {
		return agg + int(math.Pow(float64(item), 2))
	}, 0)
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}
