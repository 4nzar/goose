package goose_slice

// Flatten returns an array a single level deep.
func Flatten[T any](collection [][]T) []T {
	size := 0
	for i := range collection {
		size += len(collection[i])
	}
	result := make([]T, size)
	for i := range collection {
		result = append(result, collection[i]...)
	}
	return result
}
