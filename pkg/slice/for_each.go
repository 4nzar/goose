package goose_slice

// ForEach iterates over elements of collection
// and call iteratee for each element.
func ForEach[T any](collection []T, iteratee func(item T)) {
	for _, item := range collection {
		iteratee(item)
	}
}

// ForEach iterates over elements of collection
// and call iteratee for each element with their respective index.
func ForEachWithIndex[T any](collection []T, iteratee func(item T, index int)) {
	for i, item := range collection {
		iteratee(item, i)
	}
}
