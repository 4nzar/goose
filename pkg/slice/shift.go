package goose_slice

// removes the first item of an array.
// and return the item and the altered slice
func Shift[T any](collection []T) (T, []T) {
	var m T
	if len(collection) == 0 {
		return m, collection
	}
	result := collection[0]
	return result, collection[1:]
}
