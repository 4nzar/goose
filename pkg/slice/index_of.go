package goose_slice

// IndexOf returns the index at which the first occurrence of a value is found
// in an array or return -1 if the value is not found.
func IndexOf[T comparable](collection []T, element T) int {
	for i, item := range collection {
		if item == element {
			return i
		}
	}
	return -1
}
