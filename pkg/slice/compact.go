package goose_slice

// Compact returns a slice of all non-zero elements.
func Compact[T comparable](collection []T) []T {
	var zero T

	result := make([]T, 0, len(collection))
	for _, item := range collection {
		if item != zero {
			result = append(result, item)
		}
	}
	return result
}
