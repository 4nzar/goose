package goose_slice

// Filter iterates over elements of collection, returning an array of all
// elements predicate returns truthy for.
func Filter[T any](collection []T, predicate func(T) bool) []T {
	filtered_items := []T{}
	for _, value := range collection {
		if predicate(value) {
			filtered_items = append(filtered_items, value)
		}
	}
	return filtered_items
}

// Filter iterates over elements of collection and his index, returning an array of all
// elements predicate returns truthy for.
func FilterWithIndex[T any](collection []T, predicate func(T, int) bool) []T {
	filtered_items := []T{}
	for index, value := range collection {
		if predicate(value, index) {
			filtered_items = append(filtered_items, value)
		}
	}
	return filtered_items
}
