package goose_slice

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFilterWithSimpleDatatypeInt(t *testing.T) {
	t.Parallel()

	expect := []int{2, 4}
	got := Filter([]int{1, 2, 3, 4}, func(x int) bool {
		return x%2 == 0
	})
	assert.Equal(t, expect, got)
}

func TestFilterWithSimpleDatatypeString(t *testing.T) {
	t.Parallel()

	expect := []string{"foo", "bar"}
	got := Filter([]string{"", "foo", "", "bar", ""}, func(x string) bool {
		return len(x) > 0
	})
	assert.Equal(t, expect, got)
}

func TestFilterWithIndexWithSimpleDatatypeInt(t *testing.T) {
	t.Parallel()

	expect := []int{2, 4}
	got := FilterWithIndex([]int{1, 2, 3, 4}, func(x int, _ int) bool {
		return x%2 == 0
	})
	assert.Equal(t, expect, got)
}

func TestFilterWithIndexWithSimpleDatatypeString(t *testing.T) {
	t.Parallel()

	expect := []string{"foo", "bar"}
	got := FilterWithIndex([]string{"", "foo", "", "bar", ""}, func(x string, _ int) bool {
		return len(x) > 0
	})
	assert.Equal(t, expect, got)
}
