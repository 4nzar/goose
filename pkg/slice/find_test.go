package goose_slice

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindSimpleTestShouldReturnTrue(t *testing.T) {
	expected := 2
	test := []int{1, 2, 3, 4, 5}
	got, found := Find(test, func(item int) bool {
		return item%2 == 0
	})
	assert.True(t, found)
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}

func TestFindSimpleTestShouldReturnFalse(t *testing.T) {
	expected := ""
	test := []string{"a", "b", "c", "d"}
	got, found := Find(test, func(item string) bool {
		return item == "foobar"
	})
	assert.False(t, found)
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}
