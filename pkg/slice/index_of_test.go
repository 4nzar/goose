package goose_slice

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIndexOfSimpleTestShouldReturnRightIndex(t *testing.T) {
	expected := 2
	test := []int{0, 1, 2, 1, 2, 3}
	got := IndexOf(test, 2)
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}

func TestIndexOfSimpleTestShouldReturnMinusOne(t *testing.T) {
	expected := -1
	test := []int{1, 2, 3, 4, 5, 6, 7, 8}
	got := IndexOf(test, 200)
	assert.Equalf(t, expected, got, "Expected '%d' got '%d'\n", expected, got)
}
