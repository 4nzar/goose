package goose_slice


import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestContainsShouldReturnTrueWithSimpleDatatypeInt(t *testing.T) {
	t.Parallel()

	got := Contains([]int{1, 2, 3, 4}, 2)
	assert.True(t, got, "Expected 'true' got 'false'")
}

func TestContainsShouldReturnFalseWithSimpleDatatypeInt(t *testing.T) {
	t.Parallel()

	got := Contains([]int{1, 2, 3, 4}, 0)
	assert.False(t, got, "Expected 'false' got 'true'")
}

func TestContainsShouldReturnTrueWithSimpleDatatypeString(t *testing.T) {
	t.Parallel()

	test := strings.Split("Hello World !!", " ")
	got := Contains(test, "World")
	assert.True(t, got, "Expected 'true' got 'false'")
}

func TestContainsShouldReturnFalseWithSimpleDatatypeString(t *testing.T) {
	t.Parallel()

	test := strings.Split("Hwllo World !!", " ")
	got := Contains(test, "Hello")
	assert.False(t, got, "Expected 'false' got 'true'")
}

func TestContainsAtLeastShouldReturnTrueWithSimpleArrayOfDatatypeInt(t *testing.T) {
	t.Parallel()

	needles := []int{0, 4, 5, 10}
	got := ContainsAtLeast([]int{1, 2, 3, 4}, needles)
	assert.True(t, got, "Expected 'true' got 'false'")
}

func TestContainsAtLeastShouldReturnFalseWithSimpleArrayOfDatatypeInt(t *testing.T) {
	t.Parallel()

	needles := []int{0, 5, 10}
	got := ContainsAtLeast([]int{1, 2, 3, 4}, needles)
	assert.False(t, got, "Expected 'false' got 'true'")
}

func TestContainsAtLeastShouldReturnTrueWithSimpleArrayOfDatatypeString(t *testing.T) {
	t.Parallel()

	needles := []string{"Guys", "!!"}
	test := strings.Split("Hello World !!", " ")
	got := ContainsAtLeast(test, needles)
	assert.True(t, got, "Expected 'true' got 'false'")
}

func TestContainsAtLeastShouldReturnFalseWithSimpleArrayOfDatatypeString(t *testing.T) {
	t.Parallel()

	needles := []string{"Hello", "??"}
	test := strings.Split("Hwllo World !!", " ")
	got := ContainsAtLeast(test, needles)
	assert.False(t, got, "Expected 'false' got 'true'")
}

func TestContainsAllShouldReturnTrueWithSimpleArrayOfDatatypeInt(t *testing.T) {
	t.Parallel()

	needles := []int{2, 4}
	got := ContainsAll([]int{1, 2, 3, 4}, needles)
	assert.True(t, got, "Expected 'true' got 'false'")
}

func TestContainsAllShouldReturnFalseWithSimpleArrayOfDatatypeInt(t *testing.T) {
	t.Parallel()

	needles := []int{1, 2, 5}
	got := ContainsAll([]int{1, 2, 3, 4}, needles)
	assert.False(t, got, "Expected 'false' got 'true'")
}

func TestContainsAllShouldReturnTrueWithSimpleArrayOfDatatypeString(t *testing.T) {
	t.Parallel()

	needles := []string{"World", "!!"}
	test := strings.Split("Hello World !!", " ")
	got := ContainsAll(test, needles)
	assert.True(t, got, "Expected 'true' got 'false'")
}

func TestContainsAllShouldReturnFalseWithSimpleArrayOfDatatypeString(t *testing.T) {
	t.Parallel()

	needles := []string{"World", "!!"}
	test := strings.Split("Hello Guys !!", " ")
	got := ContainsAll(test, needles)
	assert.False(t, got, "Expected 'false' got 'true'")
}

