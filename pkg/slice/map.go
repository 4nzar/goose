package goose_slice

// Map manipulates a slice and transforms it to a slice of another type.
func Map[A, B any](collection []A, iteratee func(A) B) []B {
	mapped_items := make([]B, len(collection))
	for index, value := range collection {
		mapped_items[index] = iteratee(value)
	}
	return mapped_items
}
