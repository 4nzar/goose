package goose_slice

// Chunk returns an array of elements split into groups
// the length of size. If array can't be split evenly,
// the final chunk will be the remaining elements.
func Chunk[T any](collection []T, size uint) [][]T {
	chunks_num := len(collection) / int(size)
	if len(collection)%int(size) != 0 {
		chunks_num += 1
	}

	result := make([][]T, 0, chunks_num)

	for i := 0; i < chunks_num; i++ {
		last := (i + 1) * int(size)
		if last > len(collection) {
			last = len(collection)
		}
		offset := i * int(size)
		result = append(result, collection[offset:last])
	}

	return result
}
