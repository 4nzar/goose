package goose_slice

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForEach(t *testing.T) {
	t.Parallel()

	callParams1 := []string{}
	callParams2 := []int{}

	i := 0
	ForEach([]string{"a", "b", "c"}, func(item string) {
		callParams1 = append(callParams1, item)
		callParams2 = append(callParams2, i)
		i++
	})

	assert.ElementsMatch(t, []string{"a", "b", "c"}, callParams1)
	assert.ElementsMatch(t, []int{0, 1, 2}, callParams2)
	assert.IsIncreasing(t, callParams2)
}

func TestForEachWithIndex(t *testing.T) {
	t.Parallel()

	callParams1 := []string{}
	callParams2 := []int{}

	ForEachWithIndex([]string{"a", "b", "c"}, func(item string, i int) {
		callParams1 = append(callParams1, item)
		callParams2 = append(callParams2, i)
	})

	assert.ElementsMatch(t, []string{"a", "b", "c"}, callParams1)
	assert.ElementsMatch(t, []int{0, 1, 2}, callParams2)
	assert.IsIncreasing(t, callParams2)
}
