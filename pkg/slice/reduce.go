package goose_slice

// Reduce reduces collection to a value which is the accumulated result of
// running each element in collection through accumulator, where each successive
// invocation is supplied the return value of the previous.
func Reduce[T, R any](collection []T, accumulator func(agg R, item T) R, initial R) R {
	for _, item := range collection {
		initial = accumulator(initial, item)
	}
	return initial
}

func ReduceWithIndex[T, R any](collection []T, accumulator func(agg R, item T, index int) R, initial R) R {
	for i, item := range collection {
		initial = accumulator(initial, item, i)
	}
	return initial
}
