package goose_slice

func Contains[T comparable](collection []T, item T) bool {
	for _, value := range collection {
		if item == value {
			return true
		}
	}
	return false
}

func ContainsAtLeast[T comparable](collection []T, needles []T) bool {
	for _, value := range collection {
		for _, needle := range needles {
			if needle == value {
				return true
			}
		}
	}
	return false
}

func ContainsAll[T comparable](collection []T, needles []T) bool {
	find := 0
	for _, value := range collection {
		for _, needle := range needles {
			if needle == value {
				find++
			}
		}
	}
	return find == len(needles)
}
