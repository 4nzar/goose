package goose_http

import (
	"context"
	"net/url"
	"net/http"
	"time"

	internal_http "gitlab.com/4nzar/goose/internal/http"
	"gitlab.com/4nzar/goose/internal/model"
)

func Delete(path string, headers map[string]string, query url.Values) (*http.Response, error) {
	return DeleteWithTimeout(path, headers, query, nil)
}

func DeleteWithTimeout(path string, headers map[string]string, query url.Values, timeout *time.Duration) (*http.Response, error) {
	options := &model.RequestOptions{
		URL:             path,
		Ctx:             context.Background(),
		TimeOut:         timeout,
		Method:          model.DELETE,
		Body:            nil,
		Headers:         headers,
		QueryParameters: query,
	}
	return internal_http.Request(options)
}
