package goose_http

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"
	"io"

	"github.com/stretchr/testify/assert"
)

func TestGetRequestShouldExpect200(t *testing.T) {
	expected := "pong"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "GET", req.Method)
		assert.Equal(t, req.URL.String(), "/ping")
		rw.Write([]byte("pong"))
	}))
	defer server.Close()

	path := server.URL + "/ping"
	headers := map[string]string{}
	queries := url.Values{}
	got, err := Get(path, headers, queries)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

func TestGetRequestWithTimeout(t *testing.T) {
	expected := "ok"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, req.URL.String(), "/timeout")
		rw.Write([]byte("ok"))
	}))
	defer server.Close()
	path := server.URL + "/timeout"
	headers := map[string]string{}
	queries := url.Values{}
	timeout := time.Duration(100)
	got, err := GetWithTimeout(path, headers, queries, &timeout)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

func TestGetRequestWithQueryParameters(t *testing.T) {
	expected := "Hello john DOE"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {

		assert.Equal(t, req.URL.String(), "/greet?firstname=john&lastname=doe")
		first := req.URL.Query().Get("firstname")
		assert.Equal(t, "john", first)
		last := req.URL.Query().Get("lastname")
		assert.Equal(t, "doe", last)
		rw.Write([]byte(fmt.Sprintf("Hello %s %s", strings.ToLower(first), strings.ToUpper(last))))
	}))
	defer server.Close()

	path := server.URL + "/greet"
	headers := map[string]string{}
	queries := url.Values{}
	queries.Add("firstname", "john")
	queries.Add("lastname", "doe")
	got, err := Get(path, headers, queries)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, []byte(expected), data)
}

func TestGetRequestWithHeaders(t *testing.T) {
	expected := "Bearer foofoofoofoofoofoofoofoo"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, req.URL.String(), "/bearer")
		bearer := req.Header.Get("Authorization")
		rw.Write([]byte(bearer))
	}))
	defer server.Close()

	path := server.URL + "/bearer"
	headers := map[string]string{
		"Authorization": "Bearer foofoofoofoofoofoofoofoo",
	}
	queries := url.Values{}
	got, err := Get(path, headers, queries)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, []byte(expected), data)
}
