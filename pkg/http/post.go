package goose_http

import (
	"bytes"
	"context"
	"encoding/json"
	"net/url"
	"strconv"
	"net/http"
	"time"

	internal_http "gitlab.com/4nzar/goose/internal/http"
	"gitlab.com/4nzar/goose/internal/model"
)

func Post[T any](path string, headers map[string]string, query url.Values, data T) (*http.Response, error) {
	return PostWithTimeout(path, headers, query, data, nil)
}

func PostWithTimeout[T any](path string, headers map[string]string, query url.Values, data T, timeout *time.Duration) (*http.Response, error) {
	var buffer bytes.Buffer
	json.NewEncoder(&buffer).Encode(data)
	headers["Content-Length"] = strconv.Itoa(len(buffer.String()))
	options := &model.RequestOptions{
		URL:             path,
		Ctx:             context.Background(),
		TimeOut:         timeout,
		Method:          model.POST,
		Body:            &buffer,
		Headers:         headers,
		QueryParameters: query,
	}
	return internal_http.Request(options)
}
