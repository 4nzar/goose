package goose_http

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"
	"io"
	"io/ioutil"
	"encoding/json"

	"github.com/stretchr/testify/assert"
)

type TestPut struct {
	Username string
}

func TestPutRequestShouldExpect200(t *testing.T) {
	expected := "Hello Jdoe"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "PUT", req.Method)
		assert.Equal(t, req.URL.String(), "/update")
		req_body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		var data TestPut
		assert.Nil(t, json.Unmarshal(req_body, &data))
		rw.Write([]byte(fmt.Sprintf("Hello %s", data.Username)))
	}))
	defer server.Close()

	path := server.URL + "/update"
	headers := map[string]string{}
	queries := url.Values{}
	body := TestPut{Username: "Jdoe"}
	got, err := Put(path, headers, queries, body)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

func TestPutRequestWithTimeout(t *testing.T) {
	expected := "Hello Jdoe"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "PUT", req.Method)
		assert.Equal(t, req.URL.String(), "/update")
		req_body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		var data TestPut
		assert.Nil(t, json.Unmarshal(req_body, &data))
		rw.Write([]byte(fmt.Sprintf("Hello %s", data.Username)))
	}))
	defer server.Close()

	path := server.URL + "/update"
	headers := map[string]string{}
	queries := url.Values{}
	timeout := time.Duration(100)
	body := TestPut{Username: "Jdoe"}
	got, err := PutWithTimeout(path, headers, queries, body, &timeout)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

/*
func TestGetRequestWithQueryParameters(t *testing.T) {
	expected := "Hello john DOE"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {

		assert.Equal(t, req.URL.String(), "/greet?firstname=john&lastname=doe")
		first := req.URL.Query().Get("firstname")
		assert.Equal(t, "john", first)
		last := req.URL.Query().Get("lastname")
		assert.Equal(t, "doe", last)
		rw.Write([]byte(fmt.Sprintf("Hello %s %s", strings.ToLower(first), strings.ToUpper(last))))
	}))
	defer server.Close()

	path := server.URL + "/greet"
	headers := map[string]string{}
	queries := url.Values{}
	queries.Add("firstname", "john")
	queries.Add("lastname", "doe")
	got, err := Get(path, headers, queries)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, []byte(expected), data)
}

func TestGetRequestWithHeaders(t *testing.T) {
	expected := "Bearer foofoofoofoofoofoofoofoo"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, req.URL.String(), "/bearer")
		bearer := req.Header.Get("Authorization")
		rw.Write([]byte(bearer))
	}))
	defer server.Close()

	path := server.URL + "/bearer"
	headers := map[string]string{
		"Authorization": "Bearer foofoofoofoofoofoofoofoo",
	}
	queries := url.Values{}
	got, err := Get(path, headers, queries)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, []byte(expected), data)
}
*/
