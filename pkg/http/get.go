package goose_http

import (
	"context"
	"net/url"
	"net/http"
	"time"

	internal_http "gitlab.com/4nzar/goose/internal/http"
	"gitlab.com/4nzar/goose/internal/model"
)

func Get(path string, headers map[string]string, query url.Values) (*http.Response, error) {
	return GetWithTimeout(path, headers, query, nil)
}

func GetWithTimeout(path string, headers map[string]string, query url.Values, timeout *time.Duration) (*http.Response, error) {
	options := &model.RequestOptions{
		URL:             path,
		Ctx:             context.Background(),
		TimeOut:         timeout,
		Method:          model.GET,
		Body:            nil,
		Headers:         headers,
		QueryParameters: query,
	}
	return internal_http.Request(options)
}
