package goose_http

import (
	"bytes"
	"context"
	"encoding/json"
	"net/url"
	"net/http"
	"time"

	internal_http "gitlab.com/4nzar/goose/internal/http"
	"gitlab.com/4nzar/goose/internal/model"
)

func Put[T any](path string, headers map[string]string, query url.Values, data T) (*http.Response, error) {
	return PutWithTimeout(path, headers, query, data, nil)
}

func PutWithTimeout[T any](path string, headers map[string]string, query url.Values, data T, timeout *time.Duration) (*http.Response, error) {
	var buffer bytes.Buffer
	json.NewEncoder(&buffer).Encode(data)
	options := &model.RequestOptions{
		URL:             path,
		Ctx:             context.Background(),
		TimeOut:         timeout,
		Method:          model.PUT,
		Body:            &buffer,
		Headers:         headers,
		QueryParameters: query,
	}
	return internal_http.Request(options)
}
