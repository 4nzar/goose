package goose_http

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"
	"io"

	"github.com/stretchr/testify/assert"
)

func TestPostFormRequestShouldExpect200(t *testing.T) {
	expected := "Hello Jdoe"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, req.URL.String(), "/form")
		req.ParseForm()
		assert.Equal(t, "Jdoe", req.FormValue("username"))
		rw.Write([]byte(fmt.Sprintf("Hello %s", req.FormValue("username"))))
	}))
	defer server.Close()

	path := server.URL + "/form"
	headers := map[string]string{}
	queries := url.Values{}
	form := url.Values{}
	form.Set("username", "Jdoe")
	got, err := PostForm(path, headers, queries, form)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

func TestPostRequestWithTimeout(t *testing.T) {
	expected := "Hello Jdoe"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, req.URL.String(), "/form")
		req.ParseForm()
		assert.Equal(t, "Jdoe", req.FormValue("username"))
		rw.Write([]byte(fmt.Sprintf("Hello %s", req.FormValue("username"))))
	}))
	defer server.Close()

	path := server.URL + "/form"
	headers := map[string]string{}
	queries := url.Values{}
	form := url.Values{}
	form.Set("username", "Jdoe")
	timeout := time.Duration(100)
	got, err := PostFormWithTimeout(path, headers, queries, form, &timeout)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}
