package goose_http

import (
	"context"
	"net/url"
	"net/http"
	"strings"
	"strconv"
	"time"

	internal_http "gitlab.com/4nzar/goose/internal/http"
	"gitlab.com/4nzar/goose/internal/model"
)

func PostForm(path string, headers map[string]string, query, data url.Values) (*http.Response, error) {
	return PostFormWithTimeout(path, headers, query, data, nil)
}

func PostFormWithTimeout(path string, headers map[string]string, query, data url.Values, timeout *time.Duration) (*http.Response, error) {
	buffer := strings.NewReader(data.Encode())
	headers["Content-Type"] = "application/x-www-form-urlencoded"
	headers["Content-Length"] = strconv.Itoa(int(buffer.Size()))
	options := &model.RequestOptions{
		URL:             path,
		Ctx:             context.Background(),
		TimeOut:         timeout,
		Method:          model.POST,
		Body:            buffer,
		Headers:         headers,
		QueryParameters: query,
	}
	return internal_http.Request(options)
}
