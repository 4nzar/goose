package goose_http

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"
	"io"
	"io/ioutil"
	"encoding/json"

	"github.com/stretchr/testify/assert"
)

type TestPost struct {
	Username string
}

func TestPostRequestShouldExpect200(t *testing.T) {
	expected := "Hello Jdoe"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, req.URL.String(), "/create")
		req_body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		var data TestPost
		assert.Nil(t, json.Unmarshal(req_body, &data))
		rw.Write([]byte(fmt.Sprintf("Hello %s", data.Username)))
	}))
	defer server.Close()

	path := server.URL + "/create"
	headers := map[string]string{}
	queries := url.Values{}
	body := TestPost{Username: "Jdoe"}
	got, err := Post(path, headers, queries, body)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

func TestPostRequestWithTimeout(t *testing.T) {
	expected := "Hello Jdoe"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, req.URL.String(), "/create")
		req_body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		var data TestPost
		assert.Nil(t, json.Unmarshal(req_body, &data))
		rw.Write([]byte(fmt.Sprintf("Hello %s", data.Username)))
	}))
	defer server.Close()

	path := server.URL + "/create"
	headers := map[string]string{}
	queries := url.Values{}
	timeout := time.Duration(100)
	body := TestPost{Username: "Jdoe"}
	got, err := PostWithTimeout(path, headers, queries, body, &timeout)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

