package goose_http

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	internal_http "gitlab.com/4nzar/goose/internal/http"
	"gitlab.com/4nzar/goose/internal/model"
)

func Patch[T any](path string, headers map[string]string, query url.Values, data T) (*http.Response, error) {
	return PatchWithTimeout(path, headers, query, data, nil)
}

func PatchWithTimeout[T any](path string, headers map[string]string, query url.Values, data T, timeout *time.Duration) (*http.Response, error) {
	var buffer bytes.Buffer
	json.NewEncoder(&buffer).Encode(data)
	options := &model.RequestOptions{
		URL:             path,
		Ctx:             context.Background(),
		TimeOut:         timeout,
		Method:          model.PATCH,
		Body:            &buffer,
		Headers:         headers,
		QueryParameters: query,
	}
	return internal_http.Request(options)
}
