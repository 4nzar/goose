package goose_http

import (
//	"encoding/json"
//	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"
	"io"

	"github.com/stretchr/testify/assert"
)

func TestDeleteRequestShouldExpect200(t *testing.T) {
	expected := "pong"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "DELETE", req.Method)
		assert.Equal(t, req.URL.String(), "/ping")
		rw.Write([]byte("pong"))
	}))
	defer server.Close()

	path := server.URL + "/ping"
	headers := map[string]string{}
	queries := url.Values{}
	got, err := Delete(path, headers, queries)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

func TestDeleteRequestWithTimeout(t *testing.T) {
	expected := "ok"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "DELETE", req.Method)
		assert.Equal(t, req.URL.String(), "/timeout")
		rw.Write([]byte("ok"))
	}))
	defer server.Close()
	path := server.URL + "/timeout"
	headers := map[string]string{}
	queries := url.Values{}
	timeout := time.Duration(1000)
	got, err := DeleteWithTimeout(path, headers, queries, &timeout)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}
