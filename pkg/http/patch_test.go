package goose_http

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"
	"io"
	"io/ioutil"
	"encoding/json"

	"github.com/stretchr/testify/assert"
)

type TestPatch struct {
	Username string
}

func TestPatchRequestShouldExpect200(t *testing.T) {
	expected := "Hello Jdoe"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "PATCH", req.Method)
		assert.Equal(t, req.URL.String(), "/update")
		req_body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		var data TestPut
		assert.Nil(t, json.Unmarshal(req_body, &data))
		rw.Write([]byte(fmt.Sprintf("Hello %s", data.Username)))
	}))
	defer server.Close()

	path := server.URL + "/update"
	headers := map[string]string{}
	queries := url.Values{}
	body := TestPut{Username: "Jdoe"}
	got, err := Patch(path, headers, queries, body)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

func TestPatchRequestWithTimeout(t *testing.T) {
	expected := "Hello Jdoe"
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, "PATCH", req.Method)
		assert.Equal(t, req.URL.String(), "/update")
		req_body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		var data TestPut
		assert.Nil(t, json.Unmarshal(req_body, &data))
		rw.Write([]byte(fmt.Sprintf("Hello %s", data.Username)))
	}))
	defer server.Close()

	path := server.URL + "/update"
	headers := map[string]string{}
	queries := url.Values{}
	timeout := time.Duration(100)
	body := TestPut{Username: "Jdoe"}
	got, err := PatchWithTimeout(path, headers, queries, body, &timeout)
	assert.Nil(t, err)
	assert.Equal(t, 200, got.StatusCode)
	data, err := io.ReadAll(got.Body)
	assert.Nil(t, err)
	defer got.Body.Close()
	assert.Equal(t, expected, string(data))
}

