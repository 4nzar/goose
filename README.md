# GOOSE - collection of generic helpers

## Functions

### slice

Map:
Map manipulates a slice and transforms it to a slice of another type.
```go
func Map[A, B any](collection []A, iteratee func(A) B) []B
```

example:
```go
arr := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
result := goose_slice.Map(arr, func(item int) string {
	if item%3 == 0 && item%5 == 0 {
		return "FizzBuzz"
	}
	if item%3 == 0 {
		return "Fizz"
	}
	if item%5 == 0 {
		return "Buzz"
	}
	return strconv.Itoa(item)
})
// []string{"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz", "16", "17", "Fizz", "19", "Buzz"}
```

Find:
Find search an element in a slice based on a predicate.
It returns element and true if element was found.
```go
func Find[T any](collection []T, predicate func(item T) bool) (T, bool)
```

example:
```go
```

Filter:
Filter iterates over elements of collection, returning an array of all 
elements predicate returns truthy for.
```go
func Filter[T any](collection []T, predicate func(T) bool) []T
```
```go
func FilterWithIndex[T any](collection []T, predicate func(T, int) bool) []T
```

example:
```go
```


IndexOf:
Filter iterates over elements of collection, returning an array of all 
elements predicate returns truthy for.
```go
func Filter[T any](collection []T, predicate func(T) bool) []T
```
```go
func FilterWithIndex[T any](collection []T, predicate func(T, int) bool) []T
```

example:
```go
```

### map
*WIP*

### http

Post:
```go
func Post[T any](path string, headers map[string]string, query url.Values, data T) ([]byte, error)
```
```go
func PostWithTimeout[T any](path string, headers map[string]string, query url.Values, data T, timeout *time.Duration) ([]byte, error)
```

example:
```go
```



Get:
```go
func Get(path string, headers map[string]string, query url.Values) ([]byte, error)
```
```go
func GetWithTimeout(path string, headers map[string]string, query url.Values, timeout *time.Duration) ([]byte, error)
```

example:
```go
```

Put:
```go
func Put[T any](path string, headers map[string]string, query url.Values, data T) ([]byte, error)
```
```go
func PutWithTimeout[T any](path string, headers map[string]string, query url.Values, data T, timeout *time.Duration) ([]byte, error) 
```

example:
```go
```


Delete:
```go
func Delete(path string, headers map[string]string, query url.Values) ([]byte, error)
```
```go
func DeleteWithTimeout(path string, headers map[string]string, query url.Values, timeout *time.Duration) ([]byte, error)
```

example:
```go
```

PostForm:
```go
func PostForm(path string, headers map[string]string, query, data url.Values) ([]byte, error)
```
```go
func PostFormWithTimeout(path string, headers map[string]string, query, data url.Values, timeout *time.Duration) ([]byte, error)
```

example:
```go
```
