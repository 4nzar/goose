GOCMD			= go
BUILD_DIR		= build
BINARY_DIR		= $(BUILD_DIR)/bin
CODE_COVERAGE	= coverage

.PHONY: all
all: test.coverage

.PHONY: download
dowload: 
	@go mod download

.PHONY: test
test: download ## Run tests
	@$(GOCMD) test ./... -failfast

.PHONY: test.coverage
test.coverage: download   ## Run tests and generate coverage file
	@$(GOCMD) test -v -coverpkg ./... ./... -coverprofile=coverage.out -failfast -parallel 10

.PHONY: clean
clean:   ## Clean project
	@rm coverage.out

.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+[.]*[a-zA-Z_-]+[.]*[a-zA-Z_-]*:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
